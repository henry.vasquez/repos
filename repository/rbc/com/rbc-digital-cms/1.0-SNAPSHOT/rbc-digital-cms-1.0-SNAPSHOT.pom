<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>rbc.com</groupId>
  <artifactId>rbc-digital-cms</artifactId>
  <name>RBC Digital CMS (parent pom)</name>
  <version>1.0-SNAPSHOT</version>
  <packaging>pom</packaging>

  <properties>
    <magnoliaVersion>6.2.2</magnoliaVersion>
    <javaVersion>1.8</javaVersion>
  </properties>

  <!-- Fill the following in, so you can use the release plugin -->
  <scm>
    <connection/>
    <developerConnection/>
    <url/>
  </scm>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>info.magnolia.eebundle</groupId>
        <artifactId>magnolia-enterprise-bundle-parent</artifactId>
        <version>${magnoliaVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <!-- only necessary until MAGNOLIA-3545 is resolved -->
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-project</artifactId>
        <version>${magnoliaVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <dependency>
        <groupId>info.magnolia.rest</groupId>
        <artifactId>magnolia-rest-tools</artifactId>
        <version>2.2</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.rest</groupId>
        <artifactId>magnolia-rest-services</artifactId>
        <version>2.2</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.rest</groupId>
        <artifactId>magnolia-rest-content-delivery</artifactId>
        <version>2.2</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.rest</groupId>
        <artifactId>magnolia-rest-integration</artifactId>
        <version>2.2</version>
        <scope>compile</scope>
      </dependency>

      <dependency>
        <groupId>com.microsoft.sqlserver</groupId>
        <artifactId>mssql-jdbc</artifactId>
        <version>6.2.2.jre8</version>
      </dependency>


      <!-- TEST -->
      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.8.2</version>
        <scope>test</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>


  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>2.3.2</version>
        <configuration>
          <source>${javaVersion}</source>
          <target>${javaVersion}</target>
        </configuration>
      </plugin>
    </plugins>

    <!-- default resources configuration which will filter your module descriptors -->
    <resources>
      <resource>
        <directory>src/main/resources</directory>
        <includes>
          <include>**/*</include>
        </includes>
      </resource>
      <resource>
        <filtering>true</filtering>
        <directory>src/main/resources</directory>
        <includes>
          <include>META-INF/magnolia/*</include>
        </includes>
      </resource>
    </resources>
  </build>

  <repositories>
    <repository>
      <id>magnolia.public</id>
      <url>http://nexus.magnolia-cms.com/content/groups/public</url>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
    </repository>
    <repository>
      <id>vaadin-addons</id>
      <url>https://maven.vaadin.com/vaadin-addons</url>
    </repository>

    <!-- IF YOU NEED MODULES FROM THE ENTERPRISE VERSION, UNCOMMENT THE FOLLOWING REPOSITORY -->
    <!--
    <repository>
      <id>magnolia.enterprise.releases</id>
      <url>http://nexus.magnolia-cms.com/content/repositories/magnolia.enterprise.releases</url>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
    -->
  </repositories>

  <modules>
    <module>rbc-digital-cms-webapp</module>
    <module>rbc-author-webapp</module>
    <module>rbc-digital-cms-module</module>
  </modules>
</project>