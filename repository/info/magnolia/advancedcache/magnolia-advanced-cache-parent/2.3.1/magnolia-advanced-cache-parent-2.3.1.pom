<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>info.magnolia.maven.poms-enterprise</groupId>
    <artifactId>magnolia-parent-pom-enterprise</artifactId>
    <version>39</version>
  </parent>
  <groupId>info.magnolia.advancedcache</groupId>
  <artifactId>magnolia-advanced-cache-parent</artifactId>
  <name>Magnolia Module Advanced Cache (Parent POM)</name>
  <version>2.3.1</version>
  <packaging>pom</packaging>
  <description>A collection of advanced cache strategies</description>

  <properties>
    <javaVersion>1.8</javaVersion>
    <documentationURL>http://documentation.magnolia-cms.com/modules/advanced-cache.html</documentationURL>
    <magnoliaVersion>6.2</magnoliaVersion>
    <magnoliaUiVersion>6.2</magnoliaUiVersion>
    <cacheVersion>5.9.0</cacheVersion>
    <personalizationVersion>2.0</personalizationVersion>
    <multisiteVersion>1.3</multisiteVersion>
    <privacyVersion>1.0</privacyVersion>
  </properties>

  <issueManagement>
    <system>Jira</system>
    <url>http://jira.magnolia-cms.com/browse/MGNLADVCACHE</url>
  </issueManagement>

  <scm>
    <connection>scm:git:ssh://git@git.magnolia-cms.com/enterprise/advanced-cache</connection>
    <developerConnection>scm:git:ssh://git@git.magnolia-cms.com/enterprise/advanced-cache.git</developerConnection>
    <url>https://git.magnolia-cms.com/projects/ENTERPRISE/repos/advanced-cache</url>
    <tag>magnolia-advanced-cache-parent-2.3.1</tag>
  </scm>

  <modules>
    <module>magnolia-advanced-cache</module>
    <module>magnolia-advanced-cache-dpc</module>
    <module>magnolia-advanced-cache-app</module>
    <module>magnolia-advanced-cache-personalization</module>
  </modules>

  <dependencyManagement>
    <dependencies>
      <!-- Import dependency management for 3rd-party libraries -->
      <dependency>
        <groupId>info.magnolia.boms</groupId>
        <artifactId>magnolia-external-dependencies</artifactId>
        <version>${magnoliaVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Import dependency management section from main -->
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-project</artifactId>
        <version>${magnoliaVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Import dependency management section from UI -->
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-project</artifactId>
        <version>${magnoliaUiVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Dependencies to self -->
      <dependency>
        <groupId>info.magnolia.advancedcache</groupId>
        <artifactId>magnolia-advanced-cache</artifactId>
        <version>${project.version}</version>
      </dependency>

      <!-- Other dependencies -->
      <dependency>
        <groupId>info.magnolia.cache</groupId>
        <artifactId>magnolia-cache-core</artifactId>
        <version>${cacheVersion}</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-license</artifactId>
        <version>1.7.1</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>info.magnolia.site</groupId>
        <artifactId>magnolia-site</artifactId>
        <version>1.0</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>info.magnolia.personalization</groupId>
        <artifactId>magnolia-personalization-core</artifactId>
        <version>${personalizationVersion}</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>info.magnolia.privacy</groupId>
        <artifactId>magnolia-privacy-cookie-manager</artifactId>
        <version>${privacyVersion}</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>info.magnolia.multisite</groupId>
        <artifactId>magnolia-module-multisite</artifactId>
        <version>${multisiteVersion}</version>
        <scope>test</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <distributionManagement>
    <site>
      <id>${distribSiteId}</id>
      <url>${distribSiteRoot}/magnolia-module-advanced-cache/${project.version}/</url>
    </site>
  </distributionManagement>
</project>
