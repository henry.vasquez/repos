<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>
	<artifactId>citizens-webapp</artifactId>
	<name>Citizens Bank: Webapp</name>
	<packaging>war</packaging>
	<properties>
		<build.war.name>${client.name}WebStorefront</build.war.name>
	</properties>

	<parent>
		<groupId>com.citizens</groupId>
		<artifactId>citizens-parent</artifactId>
		<version>19-LQCD-PRODBuild</version>
		<relativePath>../citizens-parent/pom.xml</relativePath>
	</parent>

	<dependencies>

		<!-- Add your project specific dependencies here: -->

		<dependency>
			<groupId>info.magnolia.eebundle</groupId>
			<artifactId>magnolia-enterprise-pro-webapp</artifactId>
			<type>pom</type>
			<exclusions>
				<exclusion>
					<groupId>info.magnolia.demo</groupId>
					<artifactId>magnolia-travel-demo</artifactId>
				</exclusion>
				<exclusion>
					<groupId>info.magnolia.demo</groupId>
					<artifactId>magnolia-travel-tours</artifactId>
				</exclusion>
				<exclusion>
					<groupId>info.magnolia.eedemo</groupId>
					<artifactId>magnolia-travel-demo-marketing-tags</artifactId>
				</exclusion>
				<exclusion>
					<groupId>info.magnolia.eedemo</groupId>
					<artifactId>magnolia-travel-demo-multisite</artifactId>
				</exclusion>
				<exclusion>
					<groupId>info.magnolia.eedemo</groupId>
					<artifactId>magnolia-travel-demo-personalization</artifactId>
				</exclusion>
				<exclusion>
					<groupId>info.magnolia.demo</groupId>
					<artifactId>magnolia-travel-demo-enterprise</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>info.magnolia.eebundle</groupId>
			<artifactId>magnolia-enterprise-pro-webapp</artifactId>
			<type>war</type>
		</dependency>

		<!-- <dependency> <groupId>com.microsoft.sqlserver</groupId> <artifactId>mssql-jdbc</artifactId> 
			<version>6.2.2.jre8</version> <scope>test</scope> </dependency> -->

		<dependency>
			<groupId>com.citizens</groupId>
			<artifactId>citizens-module</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>com.citizens</groupId>
			<artifactId>citizens-storefront</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>info.magnolia.campaign</groupId>
			<artifactId>magnolia-campaign-publisher</artifactId>
			<version>1.3.1</version>
		</dependency>
		<dependency>
			<groupId>info.magnolia.seo</groupId>
			<artifactId>magnolia-seo</artifactId>
			<version>5.6.6</version>
		</dependency>
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>24.1.1-jre</version>
		</dependency>
	</dependencies>

	<build>
		<finalName>${build.war.name}</finalName>
		<plugins>
			<plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.6</version>
				<executions>
					<execution>
						<id>copy-resources</id>
						<!-- here the phase you need -->
						<phase>validate</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${basedir}/src/main/resources</outputDirectory>
							<resources>
								<resource>
									<includes>
										<include>version.txt</include>
									</includes>
									<directory>${basedir}</directory>
									<filtering>true</filtering>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<artifactId>maven-war-plugin</artifactId>
				<configuration>
					<!-- exclude jars copied "physically" from the webapp overlay - so we 
						only get those resolved by Maven's dependency management -->
					<dependentWarExcludes>WEB-INF/lib/*.jar</dependentWarExcludes>
				</configuration>
			</plugin>
		</plugins>
	</build>
	<profiles>
		<profile>
			<id>release</id>
			<properties>
				<build.war.name>${client.name}WebStorefront-${project.parent.version}</build.war.name>
			</properties>
		</profile>
	</profiles>
</project>
