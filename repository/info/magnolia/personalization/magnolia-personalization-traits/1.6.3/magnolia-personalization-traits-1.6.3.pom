<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <parent>
    <groupId>info.magnolia.personalization</groupId>
    <artifactId>magnolia-personalization-parent</artifactId>
    <version>1.6.3</version>
  </parent>
  <modelVersion>4.0.0</modelVersion>
  <groupId>info.magnolia.personalization</groupId>
  <artifactId>magnolia-personalization-traits</artifactId>
  <packaging>jar</packaging>

  <name>Magnolia Personalization Traits</name>

  <properties>
    <cloverCoverageThreshold>75</cloverCoverageThreshold>
  </properties>

  <dependencies>
    <dependency>
      <groupId>info.magnolia</groupId>
      <artifactId>magnolia-core</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>info.magnolia</groupId>
      <artifactId>magnolia-i18n</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-api</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-framework</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-form</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-vaadin-integration</artifactId>
    </dependency>

    <dependency>
      <groupId>info.magnolia.personalization</groupId>
      <artifactId>magnolia-personalization-integration</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>info.magnolia.personalization</groupId>
      <artifactId>magnolia-personalization-preview-app</artifactId>
      <optional>true</optional>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>info.magnolia.personalization</groupId>
      <artifactId>magnolia-personalization-segmentation-app</artifactId>
      <optional>true</optional>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>info.magnolia.personalization</groupId>
      <artifactId>magnolia-personalization-personas-app</artifactId>
      <optional>true</optional>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>info.magnolia.form</groupId>
      <artifactId>magnolia-form</artifactId>
    </dependency>

    <dependency>
      <groupId>info.magnolia.privacy</groupId>
      <artifactId>magnolia-privacy-cookie-manager</artifactId>
    </dependency>

    <dependency>
      <groupId>javax.inject</groupId>
      <artifactId>javax.inject</artifactId>
    </dependency>
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
    </dependency>
    <dependency>
      <groupId>javax.servlet.jsp</groupId>
      <artifactId>javax.servlet.jsp-api</artifactId>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.jackrabbit</groupId>
      <artifactId>jackrabbit-jcr-commons</artifactId>
    </dependency>
    <dependency>
      <groupId>com.maxmind.geoip2</groupId>
      <artifactId>geoip2</artifactId>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-server</artifactId>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-compatibility-server</artifactId>
    </dependency>
    <dependency>
      <groupId>javax.jcr</groupId>
      <artifactId>jcr</artifactId>
    </dependency>

    <!-- Tests -->
    <dependency>
      <groupId>info.magnolia.personalization</groupId>
      <artifactId>magnolia-personalization-integration</artifactId>
      <type>test-jar</type>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia</groupId>
      <artifactId>magnolia-core</artifactId>
      <type>test-jar</type>
    </dependency>
    <dependency>
      <groupId>org.mockito</groupId>
      <artifactId>mockito-core</artifactId>
    </dependency>
    <dependency>
      <groupId>org.hamcrest</groupId>
      <artifactId>hamcrest-core</artifactId>
    </dependency>
    <dependency>
      <groupId>org.hamcrest</groupId>
      <artifactId>hamcrest-library</artifactId>
    </dependency>
  </dependencies>

  <!-- Using assembly plugin to be able to bundle additional dependencies for this module -->
  <build>
    <plugins>
      <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
      </plugin>
    </plugins>
  </build>

</project>
