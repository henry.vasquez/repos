<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>info.magnolia.maven.poms-enterprise</groupId>
    <artifactId>magnolia-parent-pom-enterprise</artifactId>
    <version>36</version>
  </parent>
  <groupId>info.magnolia.tools</groupId>
  <artifactId>magnolia-tools-parent</artifactId>
  <name>Magnolia Tools Project Parent</name>
  <version>1.9.1</version>
  <packaging>pom</packaging>

  <description>Collection of useful tools to manipulate Magnolia repositories</description>

  <properties>
    <magnoliaVersion>5.6</magnoliaVersion>
    <magnoliaUiVersion>5.6</magnoliaUiVersion>
    <javaVersion>1.8</javaVersion>
  </properties>

  <issueManagement>
    <system>Jira</system>
    <url>http://jira.magnolia-cms.com/browse/MGNLTOOLS</url>
  </issueManagement>
  <inceptionYear>2008</inceptionYear>
  <scm>
    <connection>scm:git:ssh://git@git.magnolia-cms.com/enterprise/tools</connection>
    <developerConnection>scm:git:ssh://git@git.magnolia-cms.com/enterprise/tools</developerConnection>
    <url>https://git.magnolia-cms.com/projects/ENTERPRISE/repos/tools</url>
    <tag>magnolia-tools-parent-1.9.1</tag>
  </scm>

  <modules>
    <module>magnolia-tools</module>
    <module>magnolia-tools-compatibility</module>
    <module>magnolia-tools-maven-relocate</module>
  </modules>

  <dependencyManagement>
    <dependencies>
      <!-- Import 3rd-party depMan -->
      <dependency>
        <groupId>info.magnolia.boms</groupId>
        <artifactId>magnolia-external-dependencies</artifactId>
        <version>${magnoliaVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Dependency management from main -->
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-project</artifactId>
        <version>${magnoliaVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-project</artifactId>
        <version>${magnoliaUiVersion}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>

      <dependency>
        <groupId>info.magnolia.scheduler</groupId>
        <artifactId>magnolia-module-scheduler</artifactId>
        <version>2.3.1</version>
        <scope>provided</scope>
        <optional>true</optional>
      </dependency>

      <!-- Deps to reactor (self) -->
      <dependency>
        <groupId>info.magnolia.tools</groupId>
        <artifactId>magnolia-tools</artifactId>
        <version>${project.version}</version>
      </dependency>
    </dependencies>
  </dependencyManagement>
</project>
