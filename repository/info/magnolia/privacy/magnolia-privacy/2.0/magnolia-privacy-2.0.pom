<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <parent>
    <groupId>info.magnolia.maven.poms-enterprise</groupId>
    <artifactId>magnolia-parent-pom-enterprise</artifactId>
    <version>38</version>
  </parent>
  <modelVersion>4.0.0</modelVersion>
  <groupId>info.magnolia.privacy</groupId>
  <artifactId>magnolia-privacy</artifactId>
  <name>Magnolia Privacy (Parent POM)</name>
  <version>2.0</version>
  <packaging>pom</packaging>

  <properties>
    <magnoliaVersion>6.2</magnoliaVersion>
    <formVersion>2.5</formVersion>
    <cacheVersion>5.6</cacheVersion>
    <dependenciesVersion>2.0</dependenciesVersion>
    <contactAppVersion>1.6</contactAppVersion>
    <mailVersion>5.5</mailVersion>
    <javaVersion>1.8</javaVersion>
  </properties>

  <scm>
    <connection>scm:git:ssh://git@git.magnolia-cms.com/enterprise/privacy</connection>
    <developerConnection>scm:git:ssh://git@git.magnolia-cms.com/enterprise/privacy</developerConnection>
    <url>https://git.magnolia-cms.com/projects/ENTERPRISE/repos/privacy</url>
    <tag>magnolia-privacy-2.0</tag>
  </scm>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>info.magnolia.boms</groupId>
        <artifactId>magnolia-external-dependencies</artifactId>
        <version>${magnoliaVersion}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>

      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-project</artifactId>
        <version>${magnoliaVersion}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>

      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-project</artifactId>
        <version>${magnoliaVersion}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>

      <dependency>
        <groupId>info.magnolia.privacy</groupId>
        <artifactId>magnolia-privacy-cookie-manager</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.privacy</groupId>
        <artifactId>magnolia-privacy-visitor-manager</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.privacy</groupId>
        <artifactId>magnolia-privacy-form</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.privacy</groupId>
        <artifactId>magnolia-privacy-ui</artifactId>
        <version>${project.version}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.form</groupId>
        <artifactId>magnolia-form</artifactId>
        <version>${formVersion}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.cache</groupId>
        <artifactId>magnolia-cache-core</artifactId>
        <version>${cacheVersion}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.dependencies</groupId>
        <artifactId>magnolia-content-dependencies-core</artifactId>
        <version>${dependenciesVersion}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.dependencies</groupId>
        <artifactId>magnolia-content-dependencies-ui-compatibility</artifactId>
        <version>${dependenciesVersion}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.contacts</groupId>
        <artifactId>magnolia-contacts</artifactId>
        <version>${contactAppVersion}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-module-mail</artifactId>
        <version>${mailVersion}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-core</artifactId>
        <version>${magnoliaVersion}</version>
        <type>test-jar</type>
      </dependency>

    </dependencies>
  </dependencyManagement>

  <modules>
    <module>magnolia-privacy-cookie-manager</module>
    <module>magnolia-privacy-visitor-manager</module>
    <module>magnolia-privacy-form</module>
    <module>magnolia-privacy-ui</module>
    <module>magnolia-privacy-sample</module>
  </modules>

</project>
