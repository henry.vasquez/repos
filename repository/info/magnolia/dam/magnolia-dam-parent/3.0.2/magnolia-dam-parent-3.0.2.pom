<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <artifactId>magnolia-parent-pom-community</artifactId>
    <groupId>info.magnolia.maven.poms</groupId>
    <version>39</version>
  </parent>

  <groupId>info.magnolia.dam</groupId>
  <artifactId>magnolia-dam-parent</artifactId>
  <version>3.0.2</version>
  <packaging>pom</packaging>

  <name>Magnolia DAM (parent)</name>
  <description>Digital asset management (DAM) module for Magnolia.</description>

  <properties>
    <magnoliaVersion>6.2.2</magnoliaVersion>
    <magnoliaUiVersion>6.2.2</magnoliaUiVersion>
    <imagingVersion>3.4</imagingVersion>
    <javaVersion>1.8</javaVersion>
    <documentationURL>http://documentation.magnolia-cms.com/display/DOCS/Digital+Asset+Management+module</documentationURL>
    <scmTagPrefix>magnolia-dam</scmTagPrefix>
  </properties>

  <issueManagement>
    <system>Jira</system>
    <url>http://jira.magnolia-cms.com/browse/MGNLDAM</url>
  </issueManagement>
  <scm>
    <connection>scm:git:ssh://git@git.magnolia-cms.com/modules/dam</connection>
    <developerConnection>scm:git:ssh://git@git.magnolia-cms.com/modules/dam</developerConnection>
    <url>https://git.magnolia-cms.com/projects/MODULES/repos/dam</url>
    <tag>magnolia-dam-3.0.2</tag>
  </scm>

  <modules>
    <module>magnolia-dam-api</module>
    <module>magnolia-dam-core</module>
    <module>magnolia-dam-app</module>
    <module>magnolia-dam-jcr</module>
    <module>magnolia-dam-app-jcr</module>
    <module>magnolia-dam-imaging</module>
    <module>magnolia-dam-templating</module>
    <module>magnolia-dam-preview</module>
    <module>magnolia-dam-app-compatibility</module>
  </modules>

  <dependencyManagement>
    <dependencies>
      <!-- Importing dependency management from 3rd parties -->
      <dependency>
        <groupId>info.magnolia.boms</groupId>
        <artifactId>magnolia-external-dependencies</artifactId>
        <version>6.2</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Importing dependency management from magnolia-project -->
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-project</artifactId>
        <version>${magnoliaVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Importing dependency management from magnolia-ui-project -->
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-project</artifactId>
        <version>${magnoliaUiVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Deps to reactor (self) -->
      <dependency>
        <groupId>info.magnolia.dam</groupId>
        <artifactId>magnolia-dam-api</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.dam</groupId>
        <artifactId>magnolia-dam-api</artifactId>
        <version>${project.version}</version>
        <type>test-jar</type>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.dam</groupId>
        <artifactId>magnolia-dam-core</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.dam</groupId>
        <artifactId>magnolia-dam-app</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.dam</groupId>
        <artifactId>magnolia-dam-jcr</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.dam</groupId>
        <artifactId>magnolia-dam-jcr</artifactId>
        <version>${project.version}</version>
        <type>test-jar</type>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.dam</groupId>
        <artifactId>magnolia-dam-templating</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.dam</groupId>
        <artifactId>magnolia-dam-compatibility</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.dam</groupId>
        <artifactId>magnolia-dam-preview</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.dam</groupId>
        <artifactId>magnolia-dam-imaging</artifactId>
        <version>${project.version}</version>
      </dependency>

      <!-- Other deps: -->
      <dependency>
        <groupId>info.magnolia.site</groupId>
        <artifactId>magnolia-site</artifactId>
        <version>1.0.5</version>
        <scope>provided</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.imaging</groupId>
        <artifactId>magnolia-imaging-support</artifactId>
        <version>${imagingVersion}</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>info.magnolia.imaging</groupId>
        <artifactId>magnolia-imaging</artifactId>
        <version>${imagingVersion}</version>
        <scope>provided</scope>
      </dependency>

      <dependency>
        <groupId>info.magnolia.icons</groupId>
        <artifactId>magnolia-icons</artifactId>
        <version>20</version>
        <scope>provided</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <distributionManagement>
    <site>
      <id>${distribSiteId}</id>
      <url>${distribSiteRoot}/modules/magnolia-dam/${project.version}/</url>
    </site>
  </distributionManagement>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <configuration>
            <useManifestOnlyJar>false</useManifestOnlyJar>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
</project>
