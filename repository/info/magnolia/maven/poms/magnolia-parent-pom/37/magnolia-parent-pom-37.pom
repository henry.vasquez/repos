<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>info.magnolia.maven.poms</groupId>
  <artifactId>magnolia-parent-pom</artifactId>
  <packaging>pom</packaging>
  <name>magnolia-parent-pom</name>
  <version>37</version>

  <!--
    Magnolia organization-wide parent pom.
    See other poms in the poms/ folder for specialized variations.
    This pom should not be used directly as a parent for a project.
    These poms should also not have a <description> tag, as that would get inherited by projects which are too lazy to
    add their own.
  -->
  <scm>
    <connection>scm:git:http://git.magnolia-cms.com/build/poms</connection>
    <developerConnection>scm:git:https://git.magnolia-cms.com/build/poms</developerConnection>
    <url>http://git.magnolia-cms.com/gitweb/?p=build/poms.git</url>
    <tag>poms-and-tools-reactor-37</tag>
  </scm>

  <organization>
    <name>Magnolia International Ltd.</name>
    <url>http://www.magnolia-cms.com</url>
  </organization>

  <properties>
    <!-- Production code; used for -source and -target -->
    <javaVersion>1.8</javaVersion>

    <!-- Defaults to ${javaVersion}, used to tests only -->
    <javaVersionForTests>${javaVersion}</javaVersionForTests>

    <!-- By default we want to use UTF-8 encoding -->
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

    <!-- By default, SCM tags will be prefixed with the project's artifactId. Typically, in a
         multi-module project, you'll want to override this property to have more appropriate scm tags
         (e.g "foo-1.2.3" instead of "foo-parent-1.2.3") -->
    <scmTagPrefix>@{project.artifactId}</scmTagPrefix>

    <!-- These get overriden by our specialized parent poms to deploy releases to the appropriate repositories.
         The only case where a project would need to change this is if the project _was_ public and enters maintenance mode.
         In that case, set:
         <distribRepoPrefix>magnolia.public.maintenance</distribRepoPrefix>
         (See http://www.magnolia-cms.com/services/support/maintenance-policy.html) -->
    <distribRepoPrefix>magnolia.public</distribRepoPrefix>
    <distribSiteId>${distribRepoPrefix}.sites</distribSiteId>
    <distribSiteRoot>dav:https://nexus.magnolia-cms.com/content/sites/${distribRepoPrefix}.sites</distribSiteRoot>

    <!-- To enable or disable specific profiles during releases, use this property. It is to enable-clover by default. Keep in mind this only works when set on a whole project, not on sub-modules. -->
    <additionalReleaseProfiles>enable-clover</additionalReleaseProfiles>
    <!-- Use -DadditionalReleaseArguments instead of -Darguments to pass arguments to the release Maven forks -->
    <additionalReleaseArguments />

    <!-- Staging is enabled by default for the community and enterprise poms.
         To enable or disable it, set the following 3 properties need to be set accordingly (thanks Maven for the absence of boolean logic in property substitution) -->
    <nexusStagingProfileId />
    <disableNexusStaging>true</disableNexusStaging>
    <enableNexusStaging>false</enableNexusStaging>

    <!-- The coverage the build needs to reach to pass. -->
    <cloverCoverageThreshold>0</cloverCoverageThreshold>
    <!-- Ignore getter and setters by default. -->
    <cloverContextFilters>property</cloverContextFilters>
    <!-- To skip Clover in all builds, set the following property to false. Works at sub-module level, but does not prevent Clover from forking a lifecycle. -->
    <skipClover>false</skipClover>

    <!-- Set this property to false to build and attach test and test-sources jars.
         This sounds like a double-negative, but much like the disableNexusStaging/enableNexusStaging properties,
         this is due to Maven's inability to "process" properties; we can't do ${!buildTestJar}, for example.
         Both plugins involved (jar-plugin and source-plugin) use a "skip" property, so.. that's it.
         Mojo's build-helper plugin doesn't seem to be able to help either, presumably because it's too late
         to reconfigure said plugins.
     -->
    <skipTestJar>true</skipTestJar>

    <!-- Set this property to ${asciidoclet}, ${standardDoclet}, or any other Javadoc Doclet class.
         Don't forget the ${} around the property name you're referring to ! -->
    <javadocDoclet>${standardDoclet}</javadocDoclet>
    <!-- The real name of the doclet and the project is "Asciidoclet", but here's an opinionated choice: I find it less confusing and more consistent to call this property "Asciidoc Doclet": -->
    <asciidocDoclet>org.asciidoctor.Asciidoclet</asciidocDoclet>
    <standardDoclet />
    <!-- Value for javadoc's -Xdoclint option. Set to 'none' by default. Since this is only available on JDK8, this is only set via the jdk8 profile. -->
    <javadocDoclint>none</javadocDoclint>

    <!-- -D properties are normally not able to override plugin configuration from pom.xml. With these, we work around this limitation. -->
    <installAtEnd>true</installAtEnd>
    <javadocQuiet>true</javadocQuiet>

    <maven.surefire.version>3.0.0-M3</maven.surefire.version>
  </properties>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-enforcer-plugin</artifactId>
          <!-- Do not upgrade to 3.0.0-M2, it has a regression (fixed already upstream, but not released yet)
               see https://issues.apache.org/jira/browse/MENFORCER-306 -->
          <version>3.0.0-M1</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-clean-plugin</artifactId>
          <version>3.1.0</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-dependency-plugin</artifactId>
          <version>3.1.1</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>${maven.surefire.version}</version>
          <configuration>
            <systemProperties>
              <property>
                <name>java.awt.headless</name>
                <value>true</value>
              </property>
            </systemProperties>
            <!-- Use an isolate classloader -->
            <useSystemClassLoader>false</useSystemClassLoader>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-failsafe-plugin</artifactId>
          <version>${maven.surefire.version}</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-install-plugin</artifactId>
          <version>3.0.0-M1</version>
          <configuration>
            <installAtEnd>${installAtEnd}</installAtEnd>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>3.0.0-M1</version>
          <configuration>
            <deployAtEnd>true</deployAtEnd>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-release-plugin</artifactId>
          <version>2.5.3</version>
          <configuration>
            <autoVersionSubmodules>true</autoVersionSubmodules>
            <!-- This enables the "release-profile" profile when doing a release: -->
            <useReleaseProfile>true</useReleaseProfile>
            <!-- In addition, we also enable the following profiles when doing a release: -->
            <releaseProfiles>${additionalReleaseProfiles}</releaseProfiles>
            <!-- Also activate staging profile (for both :prepare and :perform.
            This overrides -Darguments; for the sake of explicitness, we introduce <additionalReleaseArguments>,
            instead of relying on ${arguments} (which also works but feels flaky) -->
            <arguments>-Pstaging ${additionalReleaseArguments}</arguments>
            <tagNameFormat>${scmTagPrefix}-@{project.version}</tagNameFormat>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.sonatype.plugins</groupId>
          <artifactId>nexus-staging-maven-plugin</artifactId>
          <version>1.6.8</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <version>3.7.1</version>
          <dependencies>
            <dependency><!-- add support for ssh/scp -->
              <groupId>org.apache.maven.wagon</groupId>
              <artifactId>wagon-webdav-jackrabbit</artifactId>
              <version>2.7</version>
            </dependency>
          </dependencies>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-javadoc-plugin</artifactId>
          <version>3.1.0</version>
          <configuration>
            <detectOfflineLinks>false</detectOfflineLinks>
            <detectLinks>false</detectLinks>
            <doclet>${javadocDoclet}</doclet>
            <quiet>${javadocQuiet}</quiet>
            <source>${javaVersion}</source>
            <!-- As long as one sets <javadocDoclet> to either ${asciidoclet} or ${standardDoclet}, we don't need other <docletArtifacts>.
                 Because of this default, other doclets should also favor the <docletArtifacts> (plural) config over the singular form to override this. -->
            <docletArtifacts>
              <docletArtifact>
                <groupId>org.asciidoctor</groupId>
                <artifactId>asciidoclet</artifactId>
                <version>1.5.4</version>
              </docletArtifact>
            </docletArtifacts>
          </configuration>
        </plugin>
        <plugin>
          <!-- project need to configure their specific descriptor(s) -->
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-assembly-plugin</artifactId>
          <version>3.1.1</version>
          <configuration>
            <tarLongFileMode>posix</tarLongFileMode>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-remote-resources-plugin</artifactId>
          <version>1.6.0</version>
        </plugin>
        <plugin>
          <!-- this is our default - sub projects can of course override this as needed -->
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>3.8.1</version>
          <configuration>
            <source>${javaVersion}</source>
            <target>${javaVersion}</target>
            <useIncrementalCompilation>false</useIncrementalCompilation>
          </configuration>
          <executions>
            <execution>
              <id>default-testCompile</id>
              <configuration>
                <source>${javaVersionForTests}</source>
                <target>${javaVersionForTests}</target>
              </configuration>
            </execution>
          </executions>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-source-plugin</artifactId>
          <version>3.1.0</version>
          <!-- if one needs to attach test-sources jar : 
                <goal>test-jar-no-fork</goal>
          -->
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-resources-plugin</artifactId>
          <version>3.1.0</version>
          <configuration>
            <encoding>UTF-8</encoding>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-jar-plugin</artifactId>
          <version>3.1.2</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-war-plugin</artifactId>
          <version>3.2.3</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-eclipse-plugin</artifactId>
          <version>2.10</version>
          <configuration>
            <downloadSources>true</downloadSources>
            <downloadJavadocs>true</downloadJavadocs>
            <wtpversion>2.0</wtpversion>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>buildnumber-maven-plugin</artifactId>
          <version>1.4</version>
          <executions>
            <execution>
              <phase>validate</phase>
              <goals>
                <goal>create</goal>
              </goals>
            </execution>
          </executions>
          <configuration>
            <!-- set those to true ? maybe only when .. installing or deploying ? -->
            <doCheck>false</doCheck>
            <doUpdate>false</doUpdate>
            <getRevisionOnlyOnce>false</getRevisionOnlyOnce>
            <buildNumberPropertyName>buildScmRevisionNumber</buildNumberPropertyName>
            <scmBranchPropertyName>buildScmBranch</scmBranchPropertyName>
          </configuration>
          <!-- if we want doCheck:true to work, we need maven-scm>=1.3
          <dependencies>
            <dependency>
              <groupId>org.apache.maven.scm</groupId>
              <artifactId>maven-scm-provider-svnexe</artifactId>
              <version>1.3</version>
            </dependency>
          </dependencies>
          -->
        </plugin>
        <plugin>
          <groupId>com.atlassian.maven.plugins</groupId>
          <artifactId>maven-clover2-plugin</artifactId>
          <version>4.0.6</version>
          <configuration>
            <jdk>${javaVersion}</jdk>
            <targetPercentage>${cloverCoverageThreshold}</targetPercentage>
            <!-- let's try both - a license file or embedded in settings.xml -->
            <licenseLocation>${cloverLicenseLocation}</licenseLocation>
            <license>${cloverLicense}</license>
            <contextFilters>${cloverContextFilters}</contextFilters>
            <!-- enable per-test coverage reports -->
            <includesTestSourceRoots>true</includesTestSourceRoots>
            <skip>${skipClover}</skip>
            <repositoryPollutionProtection>true</repositoryPollutionProtection>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-checkstyle-plugin</artifactId>
          <version>3.1.0</version>
        </plugin>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>build-helper-maven-plugin</artifactId>
          <version>3.0.0</version>
        </plugin>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>animal-sniffer-maven-plugin</artifactId>
          <version>1.18</version>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
        <executions>
          <execution>
            <id>enforce-basics</id>
            <goals>
              <goal>enforce</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <rules>
            <requireMavenVersion>
              <!-- Maven 3.2.2 suffered issues with import-scoped transitive dependencies, see: BUILD-130,
              https://issues.apache.org/jira/browse/MNG-5663 -->
              <version>[3.2.3,)</version>
            </requireMavenVersion>
            <!-- Enforce explicit version on all plugins -->
            <requirePluginVersions>
              <!-- For now, allow snapshot plugins, so that we can work on these poms -->
              <banSnapshots>false</banSnapshots>
            </requirePluginVersions>
            <!-- requireFilesDontExist currently only checks entire paths, if globs were available we'd check for >**/rebel.xml -->
          </rules>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>build-helper-maven-plugin</artifactId>
        <executions>
          <!-- Make the version parts of this project's version available as separate properties -->
          <execution>
            <id>parse-project-version</id>
            <goals>
              <goal>parse-version</goal>
            </goals>
            <configuration>
              <propertyPrefix>parsedVersion</propertyPrefix>
              <versionString>${project.version}</versionString>
            </configuration>
          </execution>
          <!--
            Make the version parts of the ${javaVersion} property
            available as separate properties, so that they can be used to define
            the java signature artifactId used by animal-sniffer, with eg "java${javaVersion.majorVersion}${javaVersion.minorVersion}"
          -->
          <execution>
            <id>parse-java-version</id>
            <goals>
              <goal>parse-version</goal>
            </goals>
            <configuration>
              <propertyPrefix>javaVersion</propertyPrefix>
              <versionString>${javaVersion}</versionString>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>animal-sniffer-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>check-java-api-compatibility</id>
            <phase>process-classes</phase>
            <goals>
              <goal>check</goal>
            </goals>
            <configuration>
              <signature>
                <groupId>org.codehaus.mojo.signature</groupId>
                <artifactId>java${javaVersion.majorVersion}${javaVersion.minorVersion}</artifactId>
                <version>1.0</version>
              </signature>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <executions>
          <execution>
            <id>build-test-jar</id>
            <goals>
              <goal>test-jar</goal>
            </goals>
            <configuration>
              <skip>${skipTestJar}</skip>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <!-- let's also attach the test sources -->
      <plugin>
        <artifactId>maven-source-plugin</artifactId>
        <executions>
          <execution>
            <id>attach-test-sources</id>
            <goals>
              <goal>test-jar-no-fork</goal>
            </goals>
            <configuration>
              <skipSource>${skipTestJar}</skipSource>
            </configuration>
          </execution>
        </executions>
      </plugin>
    </plugins>
    <!-- Our default include and filtering strategy for resources:
         * do not filter anything except the module descriptors (META-INF/magnolia/*) and license.xml
         * testResources include *.xml and *.properties from src/test/java
         If a project needs to, it can override/cancel these
         -->
    <resources>
      <resource>
        <filtering>false</filtering>
        <directory>src/main/resources</directory>
        <includes>
          <include>**/*</include>
        </includes>
        <!--
          leaving this exclude out is not a problem for maven (resources will be overridden with filtered ones in the
          next definition) and makes eclipse working. Eclipse configuration will not work with a resource dir defined
          twice with opposite excludes/includes.
          <excludes>
          <exclude>META-INF/magnolia/*</exclude>
          </excludes>
        -->
      </resource>
      <resource>
        <filtering>true</filtering>
        <directory>src/main/resources</directory>
        <includes>
          <include>META-INF/magnolia/*</include>
          <include>**/license.xml</include>
        </includes>
      </resource>
    </resources>
    <testResources>
      <testResource>
        <directory>src/test/resources</directory>
        <includes>
          <include>**/*</include>
        </includes>
      </testResource>
      <testResource>
        <directory>src/test/java</directory>
        <includes>
          <include>**/*.xml</include>
          <include>**/*.properties</include>
        </includes>
      </testResource>
    </testResources>
  </build>
  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>3.0.0</version>
        <configuration>
          <skipEmptyReport>true</skipEmptyReport>
        </configuration>
        <reportSets>
          <reportSet>
            <reports>
              <report>index</report>
              <report>summary</report>
              <report>licenses</report>
              <report>modules</report>
              <report>dependency-info</report>
              <report>dependencies</report>
              <report>dependency-management</report>
              <report>dependency-convergence</report>
              <report>distribution-management</report>
              <report>scm</report>
              <report>issue-management</report>
              <report>ci-management</report>
              <report>mailing-lists</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
      <plugin>
        <!-- two reportSets : 2 executions : aggregated and non-aggregated -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <reportSets>
          <reportSet>
            <id>aggregate</id>
            <reports>
              <report>aggregate</report>
            </reports>
          </reportSet>
          <reportSet>
            <id>non-aggregate</id>
            <reports>
              <report>javadoc</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-pmd-plugin</artifactId>
        <version>3.12.0</version>
        <configuration>
          <targetJdk>${javaVersion}</targetJdk>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jxr-plugin</artifactId>
        <version>3.0.0</version>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>clirr-maven-plugin</artifactId>
        <version>2.8</version>
        <configuration>
          <minSeverity>info</minSeverity>
          <!-- this is detected automatically by default: -->
          <!--<comparisonVersion>3.0.2</comparisonVersion>-->
        </configuration>
      </plugin>
      <!-- this report is pretty useless, since we don't allow building with failing tests ... -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-report-plugin</artifactId>
        <version>${maven.surefire.version}</version>
      </plugin>
      <!-- this report is very obscure - do we want it ? -->
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>jdepend-maven-plugin</artifactId>
        <version>2.0</version>
      </plugin>
      <plugin>
        <groupId>net.sourceforge.maven-taglib</groupId>
        <artifactId>maven-taglib-plugin</artifactId>
        <version>2.4</version>
      </plugin>
    </plugins>
  </reporting>

  <profiles>
    <profile>
      <id>jdk8+</id>
      <activation>
        <jdk>[1.8,)</jdk>
      </activation>
      <build>
        <pluginManagement>
          <plugins>
            <plugin>
              <groupId>org.apache.maven.plugins</groupId>
              <artifactId>maven-javadoc-plugin</artifactId>
              <configuration>
                <!-- -Xdoclint only exists since JDK8, which is we configure it through this profile -->
                <additionalOptions>-Xdoclint:${javadocDoclint}</additionalOptions>
              </configuration>
            </plugin>
          </plugins>
        </pluginManagement>
      </build>
    </profile>
    <profile>
      <id>show-compiler-warnings</id>
      <build>
        <pluginManagement>
          <plugins>
            <plugin>
              <groupId>org.apache.maven.plugins</groupId>
              <artifactId>maven-compiler-plugin</artifactId>
              <configuration>
                <!--<compilerArgument>-Xlint:unchecked,deprecation,fallthrough,finally</compilerArgument>-->
                <compilerArgument>-Xlint:all</compilerArgument>
                <showWarnings>true</showWarnings>
                <showDeprecation>true</showDeprecation>
              </configuration>
            </plugin>
          </plugins>
        </pluginManagement>
      </build>
    </profile>
    <profile>
      <!-- Hudson defines a couple of properties by default; we use this to enable the profile.
           http://wiki.hudson-ci.org/display/HUDSON/Building+a+software+project#Buildingasoftwareproject-HudsonSetEnvironmentVariables
      -->
      <id>continuous-integration</id>
      <activation>
        <property>
          <name>BUILD_NUMBER</name>
        </property>
      </activation>
      <build>
        <plugins>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>build-helper-maven-plugin</artifactId>
            <executions>
              <execution>
                <!-- Remove the current project's artifacts from the local repo -->
                <id>cleaning-up-local-repository</id>
                <phase>clean</phase>
                <goals>
                  <goal>remove-project-artifact</goal>
                </goals>
                <configuration>
                  <removeAll>false</removeAll>
                </configuration>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <inherited>true</inherited>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-source-plugin</artifactId>
            <executions>
              <execution>
                <id>attach-sources</id>
                <goals>
                  <goal>jar</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
    <profile>
      <!-- A profile, which, well, enables Clover. It needs to be in a separate profile, because "skipping" it still forks a second lifecycle, which we're trying to avoid.
           The profile is activated on Hudson (by the BUILD_NUMBER property), on release (see <releaseProfiles> of the maven-release-plugin), or manually at the command line.
           -->
      <id>enable-clover</id>
      <activation>
        <property>
          <!-- always enable on Hudson -->
          <name>BUILD_NUMBER</name>
        </property>
      </activation>
      <build>
        <plugins>
          <plugin>
            <groupId>com.atlassian.maven.plugins</groupId>
            <artifactId>maven-clover2-plugin</artifactId>
            <!-- As per http://docs.atlassian.com/maven-clover2-plugin/3.1.0/examples/simple.html -->
            <executions>
              <execution>
                <id>instrument-and-check</id>
                <phase>verify</phase>
                <goals>
                  <goal>instrument-test</goal>
                  <goal>log</goal>
                  <goal>check</goal>
                </goals>
              </execution>
              <execution>
                <phase>pre-site</phase>
                <goals>
                  <goal>instrument-test</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
      <reporting>
        <plugins>
          <plugin>
            <groupId>com.atlassian.maven.plugins</groupId>
            <artifactId>maven-clover2-plugin</artifactId>
          </plugin>
        </plugins>
      </reporting>
    </profile>
    <profile>
      <id>jrebel</id>
      <build>
        <plugins>
          <plugin>
            <groupId>org.zeroturnaround</groupId>
            <artifactId>jrebel-maven-plugin</artifactId>
            <version>1.1.9</version>
            <configuration>
              <addResourcesDirToRebelXml>true</addResourcesDirToRebelXml>
              <alwaysGenerate>true</alwaysGenerate>
            </configuration>
            <executions>
              <execution>
                <id>generate-rebel-xml</id>
                <phase>process-resources</phase>
                <goals>
                  <goal>generate</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
    <profile>
      <!-- A profile that binds dependency:purge-local-repository to clean, and removes info.magnolia* -->
      <id>purge-magnolia-deps</id>
      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-dependency-plugin</artifactId>
            <executions>
              <execution>
                <id>purge-local-repo-at-clean</id>
                <phase>clean</phase>
                <goals>
                  <goal>purge-local-repository</goal>
                </goals>
                <configuration>
                  <reResolve>false</reResolve>
                </configuration>
              </execution>
            </executions>
            <configuration>
              <verbose>true</verbose>
              <!-- This will remove the current project's dependencies from the local repo only if the version matches
                   (with resolutionFuzziness:artifactId or groupId, then it'd remove all versions or all artifacts of
                   the same group -->
              <resolutionFuzziness>version</resolutionFuzziness>
              <!-- Transitive but filtered to info.magnolia* -->
              <actTransitively>true</actTransitively>
              <includes>
                <!-- Includes behave a little unexpectedly: you need to include your own project,
                     as if the whole tree was filtered by these includes ... which means that this won't work if the
                     current project is not under the info.magnolia group ... -->
                <include>info.magnolia*</include>
              </includes>
            </configuration>
          </plugin>

          <!-- Also remove current project -->
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>build-helper-maven-plugin</artifactId>
            <executions>
              <execution>
                <id>purge-self-from-local-repo-at-clean</id>
                <phase>clean</phase>
                <goals>
                  <goal>remove-project-artifact</goal>
                </goals>
                <configuration>
                  <removeAll>false</removeAll>
                </configuration>
              </execution>
            </executions>
          </plugin>

        </plugins>
      </build>
    </profile>
    <profile>
      <id>staging</id>
      <!-- This profile is activated by <arguments> in the release-plugin.
      It has two features: enable the nexus-staging-maven-plugin, and the magnolia.staging.group repo.
      Both features are enabled by inverting the default value of the disableNexusStaging and enableNexusStaging
      properties, respectively. Both plugin and repository will still appear in the effective poms, but
      their skipStaging and releases/enabled properties are modified by the aforementioned properties.
      -->
      <build>
        <plugins>
          <plugin>
            <groupId>org.sonatype.plugins</groupId>
            <artifactId>nexus-staging-maven-plugin</artifactId>
            <extensions>true</extensions>
            <configuration>
              <serverId>magnolia.nexus</serverId>
              <nexusUrl>https://nexus.magnolia-cms.com</nexusUrl>
              <stagingProfileId>${nexusStagingProfileId}</stagingProfileId>
              <skipStagingRepositoryClose>false</skipStagingRepositoryClose>
              <skipStaging>${disableNexusStaging}</skipStaging>
            </configuration>
          </plugin>
        </plugins>
      </build>
      <!-- Enable staging repo group; it should be _not_ be mirrored in /staff -->
      <repositories>
        <repository>
          <id>magnolia.staging</id>
          <url>https://nexus.magnolia-cms.com/content/groups/magnolia.staging.group/</url>
          <releases>
            <enabled>${enableNexusStaging}</enabled>
          </releases>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
        </repository>
      </repositories>
    </profile>
    <profile>
      <id>loose-dependency-analysis</id>
      <build>
        <plugins>
          <plugin>
            <artifactId>maven-dependency-plugin</artifactId>
            <configuration>
              <failOnWarning>true</failOnWarning>
              <ignoredUnusedDeclaredDependencies>
                <ignoredUnusedDeclaredDependency>*</ignoredUnusedDeclaredDependency>
              </ignoredUnusedDeclaredDependencies>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>

  <distributionManagement>
    <repository>
      <id>${distribRepoPrefix}.releases</id>
      <url>https://nexus.magnolia-cms.com/content/repositories/${distribRepoPrefix}.releases</url>
    </repository>
    <snapshotRepository>
      <id>${distribRepoPrefix}.snapshots</id>
      <url>https://nexus.magnolia-cms.com/content/repositories/${distribRepoPrefix}.snapshots</url>
    </snapshotRepository>
    <!-- Project should redefine the whole <site>, but can use the snippet below. If not, every level of parent pom
         adds a path element to the final path where the site gets deployed. So we end up with, for example:
         <url>dav:https://nexus.magnolia-cms.com/content/sites/magnolia.enterprise.sites/magnolia-ldap-parent/1.5.4-SNAPSHOT/magnolia-parent-pom-abstract/magnolia-parent-pom-enterprise/magnolia-ldap-parent</url>
         If our "concrete" parent poms do this, then we already remove the worse. Sites still end up in a directory below the version directory (i.e foobar/1.2.3/foobar/).
         Multi-module projects should always redefine this so that links between the modules work properly.
         -->
    <site>
      <id>${distribSiteId}</id>
      <url>${distribSiteRoot}/${project.artifactId}/${project.version}/</url>
    </site>
  </distributionManagement>
</project>
