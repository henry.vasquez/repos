<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd ">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>info.magnolia.maven.poms-enterprise</groupId>
    <artifactId>magnolia-parent-pom-enterprise</artifactId>
    <version>39</version>
  </parent>
  <groupId>info.magnolia.diff</groupId>
  <artifactId>magnolia-diff-parent</artifactId>
  <version>2.2.1</version>
  <packaging>pom</packaging>
  <name>Magnolia Diff Parent POM</name>
  <description />

  <properties>
    <magnoliaVersion>6.2</magnoliaVersion>
    <magnoliaUiVersion>6.2</magnoliaUiVersion>
    <magnoliaPagesVersion>6.2</magnoliaPagesVersion>
    <siteVersion>1.2</siteVersion>
    <javaVersion>1.8</javaVersion>
    <documentationURL>http://documentation.magnolia-cms.com/modules/diff.html</documentationURL>
    <scmTagPrefix>magnolia-diff</scmTagPrefix>
  </properties>

  <issueManagement>
    <system>Jira</system>
    <url>http://jira.magnolia-cms.com/browse/MGNLDIFF</url>
  </issueManagement>

  <scm>
    <connection>scm:git:ssh://git@git.magnolia-cms.com/ENTERPRISE/diff</connection>
    <developerConnection>scm:git:ssh://git@git.magnolia-cms.com/ENTERPRISE/diff</developerConnection>
    <url>https://git.magnolia-cms.com/projects/ENTERPRISE/repos/diff</url>
    <tag>magnolia-diff-2.2.1</tag>
  </scm>

  <dependencyManagement>
    <dependencies>
      <!-- Import dependencies to 3rd-party libraries -->
      <dependency>
        <groupId>info.magnolia.boms</groupId>
        <artifactId>magnolia-external-dependencies</artifactId>
        <version>6.2</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Import dependency management -->
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-project</artifactId>
        <version>${magnoliaVersion}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-project</artifactId>
        <version>${magnoliaUiVersion}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>

      <!-- Dependencies to self -->
      <dependency>
        <groupId>info.magnolia.diff</groupId>
        <artifactId>magnolia-diff</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.diff</groupId>
        <artifactId>magnolia-diff-pages-integration</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.diff</groupId>
        <artifactId>magnolia-diff</artifactId>
        <version>${project.version}</version>
        <type>test-jar</type>
        <scope>test</scope>
      </dependency>


      <!-- Other dependencies -->
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-core-compatibility</artifactId>
        <version>${magnoliaVersion}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.pages</groupId>
        <artifactId>magnolia-pages-app</artifactId>
        <version>${magnoliaPagesVersion}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.site</groupId>
        <artifactId>magnolia-site</artifactId>
        <version>${siteVersion}</version>
      </dependency>

      <!-- Test dependencies -->
      <!-- Need mockito 2.x to be able mock final classes -->
      <dependency>
        <groupId>org.mockito</groupId>
        <artifactId>mockito-core</artifactId>
        <version>2.25.0</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>org.mockito</groupId>
        <artifactId>mockito-inline</artifactId>
        <version>2.25.0</version>
        <scope>test</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <modules>
    <module>magnolia-diff</module>
    <module>magnolia-diff-compatibility</module>
    <module>magnolia-diff-relocate</module>
    <module>magnolia-diff-pages-integration</module>
  </modules>
</project>
