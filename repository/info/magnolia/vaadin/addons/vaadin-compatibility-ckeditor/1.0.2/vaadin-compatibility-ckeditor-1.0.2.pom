<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <artifactId>vaadin-compatibility-addons</artifactId>
    <groupId>info.magnolia.vaadin.addons</groupId>
    <version>1.0.2</version>
    <relativePath>../pom.xml</relativePath>
  </parent>

  <artifactId>vaadin-compatibility-ckeditor</artifactId>

  <dependencies>
    <!--Needed due to vaadin8 migration script -->
    <!--This basically tries to identify vaadin version based on vaadin-shared dependency-->
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-shared</artifactId>
    </dependency>

    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-server</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-compatibility-server</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-client</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-compatibility-shared</artifactId>
    </dependency>

    <dependency>
      <groupId>org.vaadin.addons</groupId>
      <artifactId>ckeditor-wrapper-for-vaadin</artifactId>
      <version>7.12.9</version>
      <scope>provided</scope>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <!--Maven will clean generated sources upon mvn clean-->
      <plugin>
        <artifactId>maven-clean-plugin</artifactId>
        <configuration>
          <filesets>
            <fileset>
              <directory>src/main/java</directory>
              <includes>
                <include>org/vaadin/</include>
                <include>META-INF/</include>
                <include>VAADIN/</include>
                <include>*.txt</include>
              </includes>
              <excludes>
                <exclude>com/vaadin/README.txt</exclude>
              </excludes>
              <followSymlinks>false</followSymlinks>
            </fileset>
          </filesets>
        </configuration>
      </plugin>

      <!--Fetches and unpacks sources to sourceDirectory-->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-dependency-plugin</artifactId>
        <executions>
          <execution>
            <id>unpack-ckeditor-sources</id>
            <phase>generate-sources</phase>
            <goals>
              <goal>unpack</goal>
            </goals>
            <configuration>
              <artifactItems>
                <artifactItem>
                  <groupId>org.vaadin.addons</groupId>
                  <artifactId>ckeditor-wrapper-for-vaadin</artifactId>
                  <type>jar</type>
                  <!--We are only interested in sources-->
                  <overWrite>true</overWrite>
                  <outputDirectory>${project.build.sourceDirectory}</outputDirectory>
                  <!--Especially excluded since sources jar is not available of this addon and it brings .class files which-->
                  <!--eventually causes maven not to compile them after they are being patched-->
                  <excludes>**/*.class</excludes>
                </artifactItem>
              </artifactItems>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <!--Applies vaadin8 compatibility script to generated sources -->
      <plugin>
        <groupId>com.vaadin</groupId>
        <artifactId>vaadin-maven-plugin</artifactId>
        <executions>
          <execution>
            <phase>generate-sources</phase>
            <goals>
              <goal>upgrade8</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <vaadinVersion>${vaadinVersion}</vaadinVersion>
          <skip>false</skip>
        </configuration>
      </plugin>

      <!--Applies patches to generated sources-->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-patch-plugin</artifactId>
        <version>1.2</version>
        <configuration>
          <optimizations>false</optimizations>
          <patches>
            <patch>ckeditor.txt</patch>
            <patch>vckeditortextfield.txt</patch>
          </patches>
          <strictPatching>true</strictPatching>
          <strip>4</strip>
        </configuration>
        <executions>
          <execution>
            <id>patch</id>
            <phase>process-sources</phase>
            <goals>
              <goal>apply</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <!--Excludes generated sources from checkstyle -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <configuration>
          <excludes>
            org/vaadin/**,
            VAADIN/themes
          </excludes>
        </configuration>
      </plugin>
    </plugins>

    <!-- This is needed for the sources required by the GWT compiler to be
    included in the produced JARs -->
    <resources>
      <resource>
        <directory>${basedir}/src/main/java</directory>
        <excludes>
          <exclude>org/vaadin/*java</exclude>
          <exclude>VAADIN/themes</exclude>
          <exclude>.*txt</exclude>
        </excludes>
      </resource>
      <resource>
        <directory>${basedir}/src/main/resources</directory>
      </resource>
    </resources>
  </build>
</project>
