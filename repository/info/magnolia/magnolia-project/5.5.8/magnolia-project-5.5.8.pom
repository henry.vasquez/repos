<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>info.magnolia.maven.poms</groupId>
    <artifactId>magnolia-parent-pom-community</artifactId>
    <version>34</version>
  </parent>
  <groupId>info.magnolia</groupId>
  <artifactId>magnolia-project</artifactId>
  <packaging>pom</packaging>
  <name>Magnolia Main Project (Parent)</name>
  <version>5.5.8</version>
  <description>
Magnolia is an open-source enterprise class Content Management System
developed by Magnolia International Ltd., based on the standard API
for Java Content Repositories (JCR).
  </description>
  <url>http://www.magnolia-cms.com</url>
  <properties>
    <magnoliaEdition>Community Edition</magnoliaEdition>
    <magnoliaLicenseStyle>dual</magnoliaLicenseStyle>
    <documentationURL>https://documentation.magnolia-cms.com/releases/${project.version}.html</documentationURL>
    <javaVersion>1.8</javaVersion>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <scmTagPrefix>magnolia</scmTagPrefix>
  </properties>
  <issueManagement>
    <system>Jira</system>
    <url>https://jira.magnolia-cms.com/browse/MAGNOLIA</url>
  </issueManagement>
  <ciManagement>
    <notifiers>
      <notifier>
        <address>dev-list@magnolia-cms</address>
      </notifier>
    </notifiers>
  </ciManagement>
  <inceptionYear>2003</inceptionYear>
  <mailingLists>
    <mailingList>
      <name>Magnolia developer List</name>
      <subscribe>dev-list-subscribe@magnolia-cms.com</subscribe>
      <unsubscribe>dev-list-unsubscribe@magnolia-cms.com</unsubscribe>
      <archive>http://www.nabble.com/Magnolia---Dev-f650.html</archive>
    </mailingList>
    <mailingList>
      <name>Magnolia user List</name>
      <subscribe>user-list-subscribe@magnolia-cms.com</subscribe>
      <unsubscribe>user-list-unsubscribe@magnolia-cms.com</unsubscribe>
      <archive>http://www.nabble.com/Magnolia---User-f646.html</archive>
    </mailingList>
  </mailingLists>
  <scm>
    <connection>scm:git:ssh://git.magnolia-cms.com/PLATFORM/main</connection>
    <developerConnection>scm:git:ssh://git.magnolia-cms.com/PLATFORM/main</developerConnection>
    <url>https://git.magnolia-cms.com/projects/PLATFORM/repos/main</url>
    <tag>magnolia-5.5.8</tag>
  </scm>
  <build>
    <resources>
      <resource>
        <filtering>false</filtering>
        <directory>src/main/resources</directory>
        <includes>
          <include>**/*</include>
        </includes>
      </resource>
      <resource>
        <filtering>true</filtering>
        <directory>src/main/resources</directory>
        <includes>
          <include>META-INF/magnolia/*</include>
          <include>**/pddescriptor.xml</include>
        </includes>
      </resource>
      <resource>
        <filtering>false</filtering>
        <directory>${project.build.directory}/generated-resources/taglibs</directory>
        <includes>
          <include>**/*</include>
        </includes>
      </resource>
    </resources>
    <plugins>
      <plugin>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <!-- Use an isolated classloader to workaround issues where bootstrap files are not seen in tests. See BUILD-214 -->
          <useSystemClassLoader>false</useSystemClassLoader>
        </configuration>
      </plugin>
    </plugins>
    <pluginManagement>
      <plugins>
        <plugin>
          <!-- We don't want the release plugin to generate a site for this big project,
               we'll generate it manually as a separate process -->
          <artifactId>maven-release-plugin</artifactId>
          <configuration>
            <goals>deploy</goals>
          </configuration>
        </plugin>
        <!-- generate TLDs -->
        <plugin>
          <groupId>org.codehaus.xdoclet</groupId>
          <artifactId>maven2-xdoclet2-plugin</artifactId>
          <version>2.0.7</version>
          <executions>
            <execution>
              <id>xdoclet-tld</id>
              <phase>generate-sources</phase>
              <goals>
                <goal>xdoclet</goal>
              </goals>
              <configuration>
                <!-- You might need to use mvn source:jar install, or configure the source plugin
                     in taglib modules, if other modules extend the functionality of these taglibs -->
                <sourceArtifacts>info.magnolia:magnolia-taglib-*</sourceArtifacts>
                <configs>
                  <config>
                    <components>
                      <component>
                        <classname>org.xdoclet.plugin.web.TaglibPlugin</classname>
                        <params>
                          <!-- let's only generate for sources here, not the dependencies : -->
                          <restrictedpath>file://${settings.localRepository}</restrictedpath>
                          <validate>false</validate>
                          <jspversion>2.1</jspversion>
                          <!-- this will only work if we don't validate: -->
                          <taglibversion>${project.version}</taglibversion>
                          <shortname>${taglib.shortname}</shortname>
                          <uri>${taglib.uri}</uri>
                          <displayname>${project.name}</displayname>
                          <description>${project.description}</description>
                          <!--<destdir>please don't revert to the default, since it breaks mvn eclipse:eclipse - see MAGNOLIA-3070</destdir>-->
                          <destdir>${project.build.directory}/generated-resources/taglibs</destdir>
                        </params>
                      </component>
                    </components>
                    <includes>**/*.java</includes>
                    <addToSources>false</addToSources>
                    <addToResources>true</addToResources>
                  </config>
                </configs>
              </configuration>
            </execution>
          </executions>
          <dependencies>
            <!-- maven2-xdoclet2-plugin defines its own repository;
                 if we need a specific patched version of XDoclet,
                 we have to redefine these dependencies here so that
                 they're found in our own repositories.
                 The order of these dependencies is important, they
                 must be before the xdoclet plugin itself for correct
                 resolution. -->
            <!-- the xdoclet plugin itself -->
            <dependency>
              <groupId>org.codehaus.xdoclet</groupId>
              <artifactId>xdoclet-plugin-web</artifactId>
              <version>1.0.5</version>
            </dependency>
          </dependencies>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
  <dependencyManagement>
    <dependencies>
      <!-- Deps to reactor (self) -->
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-core</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-core</artifactId>
        <type>test-jar</type>
        <version>${project.version}</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.core</groupId>
        <artifactId>magnolia-resource-loader</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.core</groupId>
        <artifactId>magnolia-resource-loader</artifactId>
        <version>${project.version}</version>
        <type>test-jar</type>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.core</groupId>
        <artifactId>magnolia-configuration</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.core</groupId>
        <artifactId>magnolia-configuration</artifactId>
        <version>${project.version}</version>
        <type>test-jar</type>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-jaas</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-i18n</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-i18n</artifactId>
        <version>${project.version}</version>
        <type>test-jar</type>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.core</groupId>
        <artifactId>magnolia-freemarker-support</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-rendering</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-rendering</artifactId>
        <version>${project.version}</version>
        <type>test-jar</type>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-templating</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-templating-jsp</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.core</groupId>
        <artifactId>magnolia-virtual-uri</artifactId>
        <version>${project.version}</version>
      </dependency>

      <!-- Other deps: -->

      <dependency>
        <groupId>info.magnolia.icons</groupId>
        <artifactId>magnolia-icons</artifactId>
        <version>19</version>
      </dependency>

      <!-- Import 3rd-party depMan -->

      <dependency>
        <groupId>info.magnolia.boms</groupId>
        <artifactId>magnolia-external-dependencies</artifactId>
        <version>5.5.8</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
  <distributionManagement>
    <site>
      <id>${distribSiteId}</id>
      <url>${distribSiteRoot}/ref/${project.version}</url>
    </site>
    <downloadUrl>http://sourceforge.net/projects/magnolia/files/</downloadUrl>
  </distributionManagement>

  <!-- TODO we'll have to remove this if we want to sync our repo to central -->
  <repositories>
    <!-- just a little trick for folks who don't have our parent pom in their local repo yet -->
    <repository>
      <id>magnolia</id>
      <url>https://nexus.magnolia-cms.com/content/repositories/magnolia.public.releases/</url>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
  </repositories>

  <modules>
    <module>magnolia-core</module>
    <module>magnolia-core-compatibility</module>
    <module>magnolia-resource-loader</module>
    <module>magnolia-configuration</module>
    <module>magnolia-jaas</module>
    <module>magnolia-i18n</module>
    <module>magnolia-freemarker-support</module>
    <module>magnolia-rendering</module>
    <module>magnolia-templating</module>
    <module>magnolia-templating-jsp</module>
    <module>magnolia-virtual-uri</module>
  </modules>
</project>
