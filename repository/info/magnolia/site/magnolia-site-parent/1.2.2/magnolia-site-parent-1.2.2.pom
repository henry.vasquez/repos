<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>info.magnolia.maven.poms</groupId>
    <artifactId>magnolia-parent-pom-community</artifactId>
    <version>34</version>
  </parent>
  <groupId>info.magnolia.site</groupId>
  <artifactId>magnolia-site-parent</artifactId>
  <version>1.2.2</version>
  <name>Magnolia Site (Parent)</name>
  <packaging>pom</packaging>

  <properties>
    <magnoliaEdition>Community Edition</magnoliaEdition>
    <magnoliaLicenseStyle>dual</magnoliaLicenseStyle>
    <javaVersion>1.8</javaVersion>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <magnoliaVersion>5.6.4</magnoliaVersion>
    <magnoliaUiVersion>5.6.2</magnoliaUiVersion>
    <imagingVersion>3.4</imagingVersion>
    <resourcesVersion>2.4</resourcesVersion>
    <scmTagPrefix>magnolia-site</scmTagPrefix>
  </properties>

  <scm>
    <connection>scm:git:ssh://git@git.magnolia-cms.com/MODULES/site</connection>
    <developerConnection>scm:git:ssh://git@git.magnolia-cms.com/MODULES/site</developerConnection>
    <url>https://git.magnolia-cms.com/projects/MODULES/repos/site</url>
    <tag>magnolia-site-1.2.2</tag>
  </scm>

  <modules>
    <module>magnolia-site</module>
    <module>magnolia-site-app</module>
  </modules>

  <dependencyManagement>
    <dependencies>
      <!-- Importing dependency management from 3rd parties -->
      <dependency>
        <groupId>info.magnolia.boms</groupId>
        <artifactId>magnolia-external-dependencies</artifactId>
        <version>5.6.3</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Import dependency management section from main -->
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-project</artifactId>
        <version>${magnoliaVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Import dependency management section from UI -->
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-project</artifactId>
        <version>${magnoliaUiVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <dependency>
        <groupId>info.magnolia.imaging</groupId>
        <artifactId>magnolia-imaging-support</artifactId>
        <version>${imagingVersion}</version>
        <scope>provided</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.resources</groupId>
        <artifactId>magnolia-resources</artifactId>
        <version>${resourcesVersion}</version>
        <scope>provided</scope>
      </dependency>

      <!-- Dependencies to self -->
      <dependency>
        <groupId>info.magnolia.site</groupId>
        <artifactId>magnolia-site</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.site</groupId>
        <artifactId>magnolia-site-app</artifactId>
        <version>${project.version}</version>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <!-- We don't want the release plugin to generate a site for this big project, we'll generate it manually as a separate process -->
          <artifactId>maven-release-plugin</artifactId>
          <configuration>
            <goals>deploy</goals>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>

  <distributionManagement>
    <site>
      <id>${distribSiteId}</id>
      <url>${distribSiteRoot}/modules/site/${project.version}</url>
    </site>
  </distributionManagement>
</project>
