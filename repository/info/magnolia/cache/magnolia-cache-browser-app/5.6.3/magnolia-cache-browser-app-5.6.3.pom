<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <artifactId>magnolia-cache-parent</artifactId>
    <groupId>info.magnolia.cache</groupId>
    <version>5.6.3</version>
  </parent>
  <groupId>info.magnolia.cache</groupId>
  <artifactId>magnolia-cache-browser-app</artifactId>
  <packaging>jar</packaging>
  <name>Magnolia Cache Browser App</name>
  <description>App allowing to browse, delete and download cache entries on public instances from author instace.
  </description>

  <dependencies>
    <dependency>
      <groupId>info.magnolia</groupId>
      <artifactId>magnolia-core</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia</groupId>
      <artifactId>magnolia-i18n</artifactId>
    </dependency>

    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-api</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-contentapp</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-workbench</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-dialog</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-form</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-vaadin-integration</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia.ui</groupId>
      <artifactId>magnolia-ui-vaadin-common-widgets</artifactId>
    </dependency>

    <dependency>
      <groupId>info.magnolia.cache</groupId>
      <artifactId>magnolia-cache-core</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia.cache</groupId>
      <artifactId>magnolia-cache-app</artifactId>
    </dependency>

    <dependency>
      <groupId>info.magnolia.rest</groupId>
      <artifactId>magnolia-rest-integration</artifactId>
    </dependency>
    <dependency>
      <groupId>info.magnolia.restclient</groupId>
      <artifactId>magnolia-resteasy-client</artifactId>
    </dependency>
    <dependency>
      <groupId>io.swagger</groupId>
      <artifactId>swagger-annotations</artifactId>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
    </dependency>

    <dependency>
      <groupId>com.cedarsoftware</groupId>
      <artifactId>json-io</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>
    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
    </dependency>
    <dependency>
      <groupId>commons-beanutils</groupId>
      <artifactId>commons-beanutils</artifactId>
    </dependency>

    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
    </dependency>

    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
    </dependency>
    <dependency>
      <groupId>javax.jcr</groupId>
      <artifactId>jcr</artifactId>
    </dependency>
    <dependency>
      <groupId>javax.inject</groupId>
      <artifactId>javax.inject</artifactId>
    </dependency>
    <dependency>
      <groupId>javax.ws.rs</groupId>
      <artifactId>javax.ws.rs-api</artifactId>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-server</artifactId>
    </dependency>
    <dependency>
      <groupId>com.vaadin</groupId>
      <artifactId>vaadin-compatibility-server</artifactId>
    </dependency>

    <!-- TEST -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.mockito</groupId>
      <artifactId>mockito-core</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.hamcrest</groupId>
      <artifactId>hamcrest-core</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>info.magnolia</groupId>
      <artifactId>magnolia-core</artifactId>
      <type>test-jar</type>
      <scope>test</scope>
    </dependency>
  </dependencies>

</project>
