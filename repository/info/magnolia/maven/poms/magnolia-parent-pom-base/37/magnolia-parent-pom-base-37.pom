<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>info.magnolia.maven.poms</groupId>
    <artifactId>magnolia-parent-pom</artifactId>
    <version>37</version>
    <relativePath>../super-pom/pom.xml</relativePath>
  </parent>
  <groupId>info.magnolia.maven.poms</groupId>
  <artifactId>magnolia-parent-pom-base</artifactId>
  <name>magnolia-parent-pom-base</name>
  <packaging>pom</packaging>
  <version>37</version>
  <!--
    A base parent pom for other specialized parent poms and a shared site descriptor.
    Configures among other our own plugins and build resources.
  -->
  <properties>
    <!-- Used in various other properties and plugin configurations; one of dual, mit, cpal, mna,
         generic (avoid the latter). Our specialized parent poms already override this property. -->
    <magnoliaLicenseStyle>dual</magnoliaLicenseStyle>
    <!-- Additionally, you might want to change the below if you provide your own README.txt templates,
         checkstyle configuration etc -->
    <magnoliaBuildResourcesArtifactId>magnolia-build-resources-${magnoliaLicenseStyle}-licensed</magnoliaBuildResourcesArtifactId>
    <magnoliaBuildResourcesVersion>1.6.7</magnoliaBuildResourcesVersion>
    <checkstyleHeader>magnolia-build-resources/license-header-${magnoliaLicenseStyle}.regex</checkstyleHeader>
    <!-- If you want to use the default Checkstyle rules but ignore the license header (don't): -->
    <!-- <checkstyleHeader>magnolia-build-resources/license-header-generic.regex</checkstyleHeader> -->

    <!-- Set this to the ID of one of Maven's default assemblies or one provided by
         info.magnolia.maven.assemblies:magnolia-maven-bundle-assemblies. If you need another assembly,
         you're probably better off skipping the default assembly and re-configuring the plugin. -->
    <defaultAssemblyDescriptor>module-assembly-descriptor</defaultAssemblyDescriptor>

    <!-- This property can be used by certain bundles to drive the contents of the README.txt from
         magnolia-build-resources; currently only "tomcat-bundle" is recognized. -->
    <magnoliaProjectBundleType />

    <!-- Projects can use or try a newer version of our site skin. -->
    <mavenSiteSkinVersion>1.3.1</mavenSiteSkinVersion>

    <!-- Used for site generation; this is our generic "nexus" GA property. -->
    <googleAnalyticsAccountId>UA-6575210-21</googleAnalyticsAccountId>
  </properties>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <executions>
            <execution>
              <id>attach-descriptor</id>
              <goals>
                <goal>attach-descriptor</goal>
              </goals>
            </execution>
          </executions>
          <!-- This dependency is really just here so the skin gets included in the reactor -->
          <dependencies>
            <dependency>
              <groupId>info.magnolia.maven.siteskin</groupId>
              <artifactId>magnolia-maven-site-skin</artifactId>
              <version>${mavenSiteSkinVersion}</version>
            </dependency>
          </dependencies>
        </plugin>
        <plugin>
          <!-- Used to download Tomcat -->
          <groupId>info.magnolia.maven.plugins</groupId>
          <artifactId>magnolia-bundle-maven-plugin</artifactId>
          <version>1.1.1</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-assembly-plugin</artifactId>
          <dependencies>
            <dependency>
              <groupId>info.magnolia.maven.assemblies</groupId>
              <artifactId>magnolia-maven-bundle-assemblies</artifactId>
              <version>1.3.1</version>
            </dependency>
          </dependencies>
          <executions>
            <execution>
              <!-- Projects which need the assembly plugin with a different configuration
                   can disable this specific execution with the skipAssembly parameter;
                   See magnolia-tomcat-bundle for example -->
              <id>default-assembly</id>
              <phase>package</phase>
              <goals>
                <goal>single</goal>
              </goals>
              <configuration>
                <descriptorRefs>
                  <descriptorRef>${defaultAssemblyDescriptor}</descriptorRef>
                </descriptorRefs>
              </configuration>
            </execution>
          </executions>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-war-plugin</artifactId>
          <configuration>
            <dependentWarExcludes>WEB-INF/lib/*.jar</dependentWarExcludes>
            <webResources>
              <resource>
                <!-- these are the resources copied by the remote-resources-plugin -->
                <directory>${basedir}/target/magnolia-build-resources</directory>
              </resource>
            </webResources>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-checkstyle-plugin</artifactId>
          <dependencies>
            <dependency>
              <groupId>info.magnolia.build.resources</groupId>
              <artifactId>${magnoliaBuildResourcesArtifactId}</artifactId>
              <version>${magnoliaBuildResourcesVersion}</version>
            </dependency>
          </dependencies>
          <executions>
            <execution>
              <goals>
                <goal>check</goal>
              </goals>
            </execution>
          </executions>
          <configuration>
            <headerLocation>${checkstyleHeader}</headerLocation>
            <configLocation>magnolia-build-resources/checkstyle.xml</configLocation>
            <suppressionsLocation>magnolia-build-resources/checkstyle-suppressions.xml</suppressionsLocation>
            <enableRulesSummary>true</enableRulesSummary>
            <consoleOutput>true</consoleOutput>
            <includeTestSourceDirectory>true</includeTestSourceDirectory>
          </configuration>
        </plugin>
        <plugin>
          <!-- Managed here but configured in the super POM -->
          <artifactId>maven-dependency-plugin</artifactId>
          <version>2.10</version>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <plugin>
        <groupId>info.magnolia.maven.plugins</groupId>
        <artifactId>magnolia-setproperty-maven-plugin</artifactId>
        <version>1.2.1</version>
        <executions>
          <!-- set the current date -->
          <execution>
            <phase>generate-resources</phase>
            <id>currentDate</id>
            <goals>
              <goal>set-property</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-remote-resources-plugin</artifactId>
        <executions>
          <execution>
            <id>magnolia-build-resources-remote-resources</id>
            <goals>
              <goal>process</goal>
            </goals>
            <configuration>
              <!-- these resources are copied as per the include/excludes of the remote-resource:bundle mojo -->
              <outputDirectory>${project.build.directory}/magnolia-build-resources</outputDirectory>
              <attachToMain>true</attachToMain>
              <resourceBundles>
                <resourceBundle>info.magnolia.build.resources:${magnoliaBuildResourcesArtifactId}:${magnoliaBuildResourcesVersion}</resourceBundle>
              </resourceBundles>
              <!-- Supplemental model are used to "correct" invalid POM information from our dependencies. See BUILD-166. -->
              <supplementalModelArtifacts>
                <supplementalModelArtifact>info.magnolia.build.resources:${magnoliaBuildResourcesArtifactId}:${magnoliaBuildResourcesVersion}</supplementalModelArtifact>
              </supplementalModelArtifacts>
              <supplementalModels>
                <supplementalModel>supplemental-models.xml</supplementalModel>
              </supplementalModels>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
        <executions>
          <execution>
            <id>enforce-banned-dependencies</id>
            <goals>
              <goal>enforce</goal>
            </goals>
            <configuration>
              <rules>
                <bannedDependencies>
                  <excludes>
                    <exclude>commons-logging:*</exclude>
                    <exclude>xml-apis:*</exclude>
                    <exclude>commons-beanutils:commons-beanutils-core:*</exclude>
                    <exclude>javax.xml.stream:stax-api:*</exclude>
                    <exclude>org.apache.geronimo.specs:geronimo-stax-api_1.0_spec</exclude>
                  </excludes>
                  <includes>
                    <include>xml-apis:xml-apis:1.4.01:jar:test</include>
                    <include>xerces:xercesImpl:*:jar:test</include>
                  </includes>
                  <searchTransitive>true</searchTransitive>
                  <message><![CDATA[
-------------------------------------------------------------------------------

We try to avoid having any of the following dependencies in Magnolia projects:

* xml-apis : cause endless issues and conflicts with the xml apis already present in JDKs.
* commons-logging: Magnolia ships with SLF4J and its jcl-over-slf4j companion. For that reason, we need to avoid ending up with commons-logging in Magnolia projects. See http://slf4j.org/faq.html
* commons-beanutils:commons-beanutils-core: Magnolia ships with commons-beanutils:commons-beanutils, which contains all of commons-beanutils:commons-beanutils-core. Exclude commons-beanutils-core to avoid version conflicts.
* javax.xml.stream:stax-api : The stax-api is already present in the JDK since 1.6.
* org.apache.geronimo.specs:geronimo-stax-api_1.0_spec : The stax-api is already present in the JDK since 1.6.

Please see http://wiki.magnolia-cms.com/display/DEV/BANNED+dependencies

-------------------------------------------------------------------------------
]]>
                  </message>
                </bannedDependencies>
              </rules>
              <fail>true</fail>
            </configuration>
          </execution>
          <execution>
            <id>warn-deprecated-dependencies</id>
            <goals>
              <goal>enforce</goal>
            </goals>
            <configuration>
              <rules>
                <bannedDependencies>
                  <excludes>
                    <exclude>xerces:*</exclude>
                  </excludes>
                  <searchTransitive>true</searchTransitive>
                  <message><![CDATA[
-------------------------------------------------------------------------------

We want to avoid having any of the following dependencies in Magnolia projects in the future:

* xerces (since 5.7): we want to avoid using xerces in favor of the default implementation present in the JDK.

Remove these dependencies as they will be banned in a future releases. 

-------------------------------------------------------------------------------
]]>
                  </message>
                </bannedDependencies>
              </rules>
              <fail>false</fail>
            </configuration>
          </execution>
        </executions>
      </plugin>

    </plugins>
  </build>
</project>
