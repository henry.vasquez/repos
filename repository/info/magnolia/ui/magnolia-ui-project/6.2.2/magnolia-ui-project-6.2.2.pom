<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>info.magnolia.maven.poms</groupId>
    <artifactId>magnolia-parent-pom-community</artifactId>
    <version>39</version>
  </parent>

  <groupId>info.magnolia.ui</groupId>
  <artifactId>magnolia-ui-project</artifactId>
  <version>6.2.2</version>
  <packaging>pom</packaging>

  <name>Magnolia UI (parent)</name>
  <description />

  <properties>
    <javaVersion>1.8</javaVersion>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <magnoliaVersion>6.2.2</magnoliaVersion>
    <magnoliaTasksVersion>1.2.8</magnoliaTasksVersion>
    <imagingVersion>3.4</imagingVersion>
    <periscopeVersion>1.2.2</periscopeVersion>
    <vaadinVersion>8.9.4</vaadinVersion>
    <scmTagPrefix>magnolia-ui</scmTagPrefix>
    <magnoliaLicenseStyle>dual</magnoliaLicenseStyle>
    <contentTypeVersion>1.1.1</contentTypeVersion>
    <usageMetricsVersion>1.1.1</usageMetricsVersion>
  </properties>

  <scm>
    <connection>scm:git:ssh://git@git.magnolia-cms.com/PLATFORM/ui</connection>
    <developerConnection>scm:git:ssh://git@git.magnolia-cms.com/PLATFORM/ui</developerConnection>
    <url>https://git.magnolia-cms.com/projects/PLATFORM/repos/ui</url>
    <tag>magnolia-ui-6.2.2</tag>
  </scm>

  <modules>
    <module>magnolia-ui-api</module>
    <module>magnolia-ui-vaadin-common-widgets</module>
    <module>magnolia-ui-vaadin-theme</module>

    <module>magnolia-ui-mediaeditor</module>

    <module>magnolia-ui-framework-compatibility</module>

    <module>relocations/magnolia-ui-actionbar</module>
    <module>relocations/magnolia-ui-contentapp</module>
    <module>relocations/magnolia-ui-dialog</module>
    <module>relocations/magnolia-ui-form</module>
    <module>relocations/magnolia-ui-imageprovider</module>
    <module>relocations/magnolia-ui-workbench</module>
    <module>relocations/magnolia-ui-vaadin-integration</module>

    <module>magnolia-ui-framework</module>
    <module>magnolia-ui-framework-jcr</module>

    <module>magnolia-resurface-theme</module>
    <module>magnolia-admincentral</module>

    <module>magnolia-ui-admincentral</module>
    <module>about-app</module>
    <module>sample-app</module>
    <module>messages-app</module>
    <module>security-app</module>
    <module>tasks-app</module>
    <module>jcr-browser-app</module>

    <module>ui-test-webapp</module>
  </modules>

  <dependencyManagement>
    <dependencies>

      <!-- Importing dependency management from 3rd parties -->
      <dependency>
        <groupId>info.magnolia.boms</groupId>
        <artifactId>magnolia-external-dependencies</artifactId>
        <version>${magnoliaVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Dependency management from main -->
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-project</artifactId>
        <version>${magnoliaVersion}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Deps to reactor (self) -->
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-api</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-api</artifactId>
        <type>test-jar</type>
        <scope>test</scope>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-vaadin-integration</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-vaadin-common-widgets</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-vaadin-theme</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-actionbar</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-mediaeditor</artifactId>
        <version>${project.version}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-framework-compatibility</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-framework-compatibility</artifactId>
        <version>${project.version}</version>
        <type>test-jar</type>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-contentapp</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-dialog</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-form</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-form</artifactId>
        <type>test-jar</type>
        <scope>test</scope>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-imageprovider</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-workbench</artifactId>
        <version>${project.version}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-framework</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-framework</artifactId>
        <type>test-jar</type>
        <scope>test</scope>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-framework-jcr</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-framework-jcr</artifactId>
        <version>${project.version}</version>
        <type>test-jar</type>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-resurface-theme</artifactId>
        <version>${project.version}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-admincentral</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-admincentral</artifactId>
        <type>test-jar</type>
        <scope>test</scope>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.about</groupId>
        <artifactId>magnolia-about-app</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.sample</groupId>
        <artifactId>magnolia-sample-app</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.messages</groupId>
        <artifactId>magnolia-messages-app</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.security</groupId>
        <artifactId>magnolia-security-app</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.jcrbrowser</groupId>
        <artifactId>magnolia-jcr-browser-app</artifactId>
        <version>${project.version}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.task</groupId>
        <artifactId>magnolia-tasks-app</artifactId>
        <version>${project.version}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.usagemetrics</groupId>
        <artifactId>magnolia-usage-metrics</artifactId>
        <version>${usageMetricsVersion}</version>
      </dependency>

      <!-- Other deps: -->
      <dependency>
        <groupId>info.magnolia.icons</groupId>
        <artifactId>magnolia-icons</artifactId>
        <version>22</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.task</groupId>
        <artifactId>magnolia-task-management</artifactId>
        <version>${magnoliaTasksVersion}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.imaging</groupId>
        <artifactId>magnolia-imaging</artifactId>
        <version>${imagingVersion}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.scheduler</groupId>
        <artifactId>magnolia-module-scheduler</artifactId>
        <version>2.3</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia.periscope</groupId>
        <artifactId>magnolia-periscope-core</artifactId>
        <version>${periscopeVersion}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.periscope</groupId>
        <artifactId>magnolia-periscope-api</artifactId>
        <version>${periscopeVersion}</version>
      </dependency>
      <dependency>
        <groupId>info.magnolia.periscope</groupId>
        <artifactId>magnolia-speech-recognition</artifactId>
        <version>${periscopeVersion}</version>
      </dependency>

    </dependencies>
  </dependencyManagement>

  <dependencies>
    <dependency>
      <groupId>org.vaadin.addon</groupId>
      <artifactId>easyuploads</artifactId>
    </dependency>
  </dependencies>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-patch-plugin</artifactId>
          <version>1.1.1</version>
        </plugin>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>groovy-maven-plugin</artifactId>
          <version>1.4</version>
        </plugin>
        <plugin>
          <groupId>com.vaadin</groupId>
          <artifactId>vaadin-maven-plugin</artifactId>
          <!--<configuration>-->
            <!--<logLevel>DEBUG</logLevel>-->
          <!--</configuration>-->
          <version>${vaadinVersion}</version>
        </plugin>
        <plugin>
          <!-- We don't want the release plugin to generate a site for this big project, we'll generate it manually as a separate process -->
          <artifactId>maven-release-plugin</artifactId>
          <configuration>
            <goals>deploy</goals>
          </configuration>
        </plugin>
        <plugin>
          <!-- Use an isolated classloader to workaround issues where bootstrap files are not seen in tests. See BUILD-214 -->
          <artifactId>maven-surefire-plugin</artifactId>
          <configuration>
            <useSystemClassLoader>false</useSystemClassLoader>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>

  <distributionManagement>
    <site>
      <id>${distribSiteId}</id>
      <url>${distribSiteRoot}/ui/${project.version}</url>
    </site>
  </distributionManagement>

  <repositories>
    <repository>
      <id>vaadin.addons</id>
      <url>https://maven.vaadin.com/vaadin-addons/</url>
    </repository>
  </repositories>
</project>
