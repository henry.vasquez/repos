<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd ">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>info.magnolia.maven.poms-enterprise</groupId>
    <artifactId>magnolia-parent-pom-enterprise</artifactId>
    <version>37</version>
  </parent>
  <groupId>info.magnolia.translation</groupId>
  <artifactId>magnolia-content-translation-support-parent</artifactId>
  <version>2.3.1</version>
  <packaging>pom</packaging>
  <name>Magnolia Content Translation Support Parent POM</name>
  <description>
   This version is compatible with Magnolia 5.7.3 and greater.
   As of Magnolia 6.1, please use version 2.4
 </description>

  <properties>
    <magnoliaVersion>5.7.3</magnoliaVersion>
    <magnoliaUiVersion>5.7.3</magnoliaUiVersion>
    <magnoliaPagesVersion>5.7.1</magnoliaPagesVersion>
    <javaVersion>1.8</javaVersion>
    <blossomVersion>3.2</blossomVersion>
    <scmTagPrefix>magnolia-content-translation-support</scmTagPrefix>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>

  <issueManagement>
    <system>Jira</system>
    <url>http://jira.magnolia-cms.com/browse/MGNLCTS</url>
  </issueManagement>

  <scm>
    <connection>scm:git:ssh://git@git.magnolia-cms.com/ENTERPRISE/content-translation-support</connection>
    <developerConnection>scm:git:ssh://git@git.magnolia-cms.com/ENTERPRISE/content-translation-support</developerConnection>
    <url>https://git.magnolia-cms.com/projects/ENTERPRISE/repos/content-translation-support</url>
    <tag>magnolia-content-translation-support-2.3.1</tag>
  </scm>

  <modules>
    <module>magnolia-content-translation</module>
    <module>magnolia-content-translation-pages-integration</module>
    <module>magnolia-content-translation-support-compatibility</module>
    <module>magnolia-content-translation-support-relocation</module>
  </modules>

  <dependencyManagement>
    <dependencies>
      <!-- Import dependencies to 3rd-party libraries -->
      <dependency>
        <groupId>info.magnolia.boms</groupId>
        <artifactId>magnolia-external-dependencies</artifactId>
        <version>5.7.2</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- Dependency management import for main/UI -->
      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-project</artifactId>
        <version>${magnoliaVersion}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>info.magnolia.ui</groupId>
        <artifactId>magnolia-ui-project</artifactId>
        <version>${magnoliaUiVersion}</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>

      <!-- Dependencies to self -->
      <dependency>
        <groupId>info.magnolia.translation</groupId>
        <artifactId>magnolia-content-translation</artifactId>
        <version>${project.version}</version>
      </dependency>

      <!-- Other dependencies -->
      <dependency>
        <groupId>info.magnolia.site</groupId>
        <artifactId>magnolia-site</artifactId>
        <version>1.0</version>
        <scope>provided</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.blossom</groupId>
        <artifactId>magnolia-module-blossom</artifactId>
        <version>${blossomVersion}</version>
        <scope>provided</scope>
      </dependency>
      <dependency>
        <groupId>info.magnolia.pages</groupId>
        <artifactId>magnolia-pages-app</artifactId>
        <version>${magnoliaPagesVersion}</version>
      </dependency>

      <dependency>
        <groupId>info.magnolia</groupId>
        <artifactId>magnolia-license</artifactId>
        <version>1.4.3</version>
        <scope>provided</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
</project>
